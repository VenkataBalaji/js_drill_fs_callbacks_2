/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

// let boards=require("./boards.json")



function getData(lists,id,callback){
    setTimeout(() => {
      let listsData= lists[id]
      callback(listsData)
    }, 2000);
}

module.exports=getData